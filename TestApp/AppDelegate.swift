//
//  AppDelegate.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import VRGSoftIOSNetworkKit
import VRGSoftIOSDBKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        SMGatewayConfigurator.shared.url = URL(string: APPConfig.baseUrl)
        SMDBStorageConfigurator.registerStorageClass(MainStorage.self)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        AppManager.shared.updateRootViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
}

