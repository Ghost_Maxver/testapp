//
//  MainStorage.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import Foundation
import VRGSoftIOSDBKit

class MainStorage: SMDBStorage {

    override func persistentStoreName() -> String? {
        
        return "DataModel"
    }
}
