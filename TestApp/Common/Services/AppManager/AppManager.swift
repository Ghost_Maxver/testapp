//
//  AppManager.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import UIKit
import VRGSoftIOSDBKit
import VRGSoftIOSNetworkKit

struct AppManagerUDKeys {
    
    static let session: String = "AppManagerUDKeysSession"
    static let showedStartScreen: String = "AppManagerUDKeysShowedStartScreen"
}

struct AppManagerNotificationsKeys {
    
    static let session: String = "AppManagerNotificationsKeys.session"
}

class AppManager {
    
    static let shared: AppManager = AppManager()
    
    var user: BOUser? {
        return SessionManager.shared.user
    }
    
    var isLogined: Bool {
        return SessionManager.shared.isLogined
    }
        
    let route: Route = Route(window: UIApplication.shared.delegate!.window!!) // swiftlint:disable:this force_unwrapping
    
    func updateRootViewController() {

        if isLogined {

            route.switchToHome()
        } else {

            route.switchToAuth()
        }
    }
    
    func loginWithSession(_ aSession: Session) {

        SessionManager.shared.loginWithSession(aSession)
        updateGateways()
    }
    
    func logout() {

        SessionManager.shared.logout()
        DispatchQueue.main.async {
            AppManager.shared.updateGateways()
        }
        AppManager.shared.updateRootViewController()
        SMDBStorageConfigurator.storage?.clear()
    }
    
    func updateGateways() {

        if let token: String = SessionManager.shared.session?.token {

            let header: String = "Bearer " + token
            SMGatewayConfigurator.shared.setHTTPHeader(value: header, key: "Authorization")
        } else {

            SMGatewayConfigurator.shared.setHTTPHeader(value: nil, key: "Authorization")
        }
    }
}

extension Route {
    
    func switchToHome() {
        // TODO: need add vc
    }
    
    func switchToAuth() {
        // TODO: need add vc
    }
}
