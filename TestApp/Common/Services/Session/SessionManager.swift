//
//  SessionManager.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import Foundation

struct SessionManagerUDKeys {

    static let session: String = "SMAppManagerUDKeysSession"
}

class SessionManager {

    static let shared: SessionManager = SessionManager()
    
    var session: Session?
    
    var user: BOUser? {

        if let userID: NSNumber = session?.userID {

            let user: BOUser? = BOUser.objectByID(userID)
            return user
        } else {

            return nil
        }
    }
    
    var isLogined: Bool {

        return session != nil
    }
    
    init() {

        session = UserDefaults.standard.coding(forKey: SessionManagerUDKeys.session)
    }
    
    func loginWithSession(_ aSession: Session) {

        session = aSession
        saveSession()
    }
    
    func logout() {

        removeSession()
    }
    
    private func saveSession() {

        if let session: Session = session {

            let ud: UserDefaults = UserDefaults.standard
            ud.setCoding(session, forKey: SessionManagerUDKeys.session)
            ud.synchronize()
        }
    }
    
    private func removeSession() {

        session = nil
        let ud: UserDefaults = UserDefaults.standard
        ud.removeObject(forKey: SessionManagerUDKeys.session)
        ud.synchronize()
    }
}
