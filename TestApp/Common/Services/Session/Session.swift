//
//  Session.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import Foundation

struct SessionUDKey {

    static let token: String = "SMSessionKeyToken"
    static let userID: String = "SMSessionKeyUserID"
}


class Session: NSObject, NSCoding {

    var userID: NSNumber
    let token: String

    init(token aToken: String, userID aUserID: NSNumber) {

        token = aToken
        userID = aUserID
    }
    
    
    // MARK: - NSCoding
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(token, forKey: SessionUDKey.token)
        aCoder.encode(userID, forKey: SessionUDKey.userID)
    }
    
    required init?(coder aDecoder: NSCoder) {

        if  let token: String = aDecoder.decodeObject(forKey: SessionUDKey.token) as? String,
            let userID: NSNumber = aDecoder.decodeObject(forKey: SessionUDKey.userID) as? NSNumber {

            self.token =  token
            self.userID =  userID
        } else {

            return nil
        }
    }
}
