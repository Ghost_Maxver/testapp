//
//  AuthGateway.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import UIKit
import VRGSoftIOSNetworkKit
import SwiftyJSON
import Alamofire

class AuthGateway: SMBaseGateway {
    
    static let shared: AuthGateway = AuthGateway()
    
    private func loginWithJson(_ json: JSON) {
        if let token: String = json["access_token"].string, let userID: NSNumber = json["uid"].number {

            let session: Session = Session(token: token, userID: userID)
            AppManager.shared.loginWithSession(session)
        }
    }
}
