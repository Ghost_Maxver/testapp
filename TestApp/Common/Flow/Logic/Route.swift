//
//  Route.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import UIKit

class Route {
    
    let window: UIWindow
    
    init(window aWindow: UIWindow) {
        
        window = aWindow
    }
    
    func switchTo(vc aVc: UIViewController) {
        
        let snapShot: UIView? = window.snapshotView(afterScreenUpdates: false)
        if let snapShot: UIView = snapShot {
            
            self.window.addSubview(snapShot)
        }
        
        if let rootViewController: UIViewController = window.rootViewController {
            
            self.dismiss(vc: rootViewController) {
                
                self.window.rootViewController = aVc
                
                if let snapShot: UIView = snapShot {
                    
                    self.window.bringSubviewToFront(snapShot)
                    UIView.animate(withDuration: 0.3, animations: {
                        snapShot.layer.opacity = 0
                    }, completion: { _ in
                        DispatchQueue.main.async {
                            snapShot.removeFromSuperview()
                        }
                    })
                }
            }
        } else {
            
            window.rootViewController = aVc
        }
    }
    
    func dismiss(vc aVc: UIViewController, completion aCallBack: @escaping (() -> Swift.Void)) {
        
        if let vc: UIViewController = aVc.presentedViewController {
            
            self.dismiss(vc: vc, completion: {
                aVc.dismiss(animated: true, completion: {
                    aCallBack()
                })
            })
        } else {
            aCallBack()
        }
    }
}
