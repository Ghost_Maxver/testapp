import Foundation
import VRGSoftIOSDBKit

@objc(BOUser)
open class BOUser: _BOUser, SMDBStorableObject {
	
    
    
    public static var primaryKey: String {
        return "uid"
    }
}
