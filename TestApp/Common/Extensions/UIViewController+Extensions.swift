//
//  UIViewController+Extensions.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import UIKit

extension UIViewController {
    
    static func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController: UINavigationController = controller as? UINavigationController {
            
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let tabController: UITabBarController = controller as? UITabBarController {
            
            if let selected: UIViewController = tabController.selectedViewController {
                
                return topViewController(controller: selected)
            }
        }
        
        if let presented: UIViewController = controller?.presentedViewController {
            
            return topViewController(controller: presented)
        }
        
        return controller
    }
    
    @discardableResult
    func showAlertController(title aTitle: String?, message aMessage: String?, cancelButtonTitle aCancelButtonTitle: String?) -> UIViewController? {
        
        if aTitle == nil && aMessage == nil {
            
            return nil
        }
        
        return UIViewController.showAlert(title: aTitle, message: aMessage, cancelTitleButton: aCancelButtonTitle ?? "OK", otherTitleButtons: nil, completion: nil)
        
    }
    
    @discardableResult
    static func showAlert(title: String? = nil, message: String? = nil, cancelTitleButton: String = "OK", otherTitleButtons: [String]? = nil, completion: ((Int) ->())? = nil) -> UIViewController {
        
        let alert: UIAlertController = .init(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction: UIAlertAction = .init(title: cancelTitleButton, style: .cancel) { _ in
            completion?(0)
        }
        
        alert.addAction(cancelAction)
        
        if let otherTitleButtons: [String] = otherTitleButtons {
            for (index, title) in otherTitleButtons.enumerated() {
                let action: UIAlertAction = .init(title: title, style: .default) { _ in
                    completion?(index + 1)
                }
                alert.addAction(action)
            }
        }
        
        if let topController: UIViewController = topViewController() {

            topController.present(alert, animated: true, completion: nil)
        }
        
        return alert
    }
}
