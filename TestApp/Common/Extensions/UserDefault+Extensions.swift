//
//  UserDefault+Extensions.swift
//  TestApp
//
//  Created by developer on 18.03.2020.
//

import Foundation

extension UserDefaults {
    
    func setCoding(_ aCoding: NSCoding, forKey aKey: String) {
        
        let data: Data = NSKeyedArchiver.archivedData(withRootObject: aCoding)
        set(data, forKey: aKey)
    }
    
    func coding<T: NSCoding>(forKey aKey: String) -> T? {
        
        var result: T?
        
        let data: Any? = object(forKey: aKey)
        
        if let data: Data = data as? Data {
            result = NSKeyedUnarchiver.unarchiveObject(with: data) as? T
        }
        
        return result
    }
}
